from main import OtodomParser

if __name__ == '__main__':
    url = 'https://www.otodom.pl/sprzedaz/mieszkanie/warszawa/' \
          '?search%5Bfilter_float_price%3Ato%5D=450000&search%5Bfilter_float_m' \
          '%3Afrom%5D=50&search%5Bdist%5D=0&search%5Bsubregion_id%5D=197&search%5Bcity_id%5D=26&nrAdsPerPage=72'

    parser = OtodomParser(url, 28)
    parser.parse()
