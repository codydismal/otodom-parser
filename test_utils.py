arcticle = """<article class="offer-item ad_id3USQv" data-featured-name="promo_vip" data-item-id="3USQv" data-tracking-id="57886208" data-url="https://www.otodom.pl/oferta/wyjatkowe-3-pok-83-m2-wola-mirow-ID3USQv.html#5696510837">
<figure class="offer-item-image" data-featured-name="no_offer">
<a data-featured-name="promo_vip" data-tracking="click_body" data-tracking-data='{"touch_point_button":"photo"}' href="https://www.otodom.pl/oferta/wyjatkowe-3-pok-83-m2-wola-mirow-ID3USQv.html#5696510837">
<span class="img-cover" style="background-image:url(https://apollo-ireland.akamaized.net/v1/files/eyJmbiI6ImlqcjE1NWN2ZWI0eC1BUEwiLCJ3IjpbeyJmbiI6ImoxajNvMTNtNmJnbjEtQVBMIiwicyI6IjE0IiwicCI6IjEwLC0xMCIsImEiOiIwIn1dfQ.gSgCfBpSdfUI1xBBIShZfOGsB7HlnLg6Zn2paMOlh2w/image;s=655x491;q=80)" title="Mieszkanie na sprzedaż o powierzchni 83 m² - Warszawa, Wola - zdjęcie 1"></span>
<span class="promoted-label" data-label="Tylko w Otodom"></span>
</a>
</figure>
<div class="offer-item-details">
<img alt="" class="offer-item-business-brand" src="https://apollo-ireland.akamaized.net/v1/files/eyJmbiI6IjZ4cGdjZjBnY3B0MjMtQVBMIn0.-eOEn4VxuDFe0Ik8E3E8BjjWEnNopbTv1LKqo8E_otM/image;s=219x163;q=80"/>
<header class="offer-item-header">
<h3>
<a data-featured-name="promo_vip" data-tracking="click_body" data-tracking-data='{"touch_point_button":"title"}' href="https://www.otodom.pl/oferta/wyjatkowe-3-pok-83-m2-wola-mirow-ID3USQv.html#5696510837">
<strong class="visible-xs-block">83 m²</strong>
<span class="text-nowrap">
<span class="offer-item-title">Wyjątkowe 3 pok. 83 m2 Wola Mirów</span>
</span>
</a>
</h3>
<p class="text-nowrap hidden-xs">Mieszkanie na sprzedaż: Warszawa, Wola</p>
</header>
<ul class="params" data-tracking="click_body" data-tracking-data='{"touch_point_button":"body"}'>
<li class="offer-item-rooms hidden-xs">3 pokoje</li>
<li class="offer-item-price">
                                                    730 000 zł                                                                                            </li>
<li class="hidden-xs offer-item-area">83 m²</li>
<li class="hidden-xs offer-item-price-per-m">8 795 zł/m²</li>
</ul>
</div>
<div class="offer-item-details-bottom" data-tracking="click_body" data-tracking-data='{"touch_point_button":"body"}'>
</div>
<div class="offer-item-observe-button vas-list-no-offer">
<a class="button-observed observe-link favourites-button" data-id="57886208" data-statkey="ad.observed.list" href="#" rel="nofollow">
<span class="icon observed-57886208"></span>
<svg height="100%" version="1.1" viewbox="0 0 52 47" width="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<!-- Generator: Sketch 51.2 (57519) - http://www.bohemiancoding.com/sketch -->
<title>Group 7@SVG</title>
<desc>Created with Sketch.</desc>
<defs></defs>
<g fill-rule="evenodd" id="14_SubAccounts" stroke="none" stroke-width="1">
<g fill-rule="nonzero" id="#-styleguide" transform="translate(-290.000000, -3431.000000)">
<g id="heart-regular" transform="translate(290.000000, 3417.000000)">
<g id="Group-7" transform="translate(0.000000, 14.000000)">
<g id="Group-6">
<path d="M44.9749792,5.0327098 C39.5562332,0.472655041 31.1843643,1.15760154 25.9999932,6.44013411 C20.815622,1.15760154 12.4437532,0.463272213 7.0250072,5.0327098 C-0.0249875872,10.9720404 1.00626165,20.6551196 6.03125794,25.7875269 L22.4749958,42.5546419 C23.4124951,43.5116904 24.6687442,44.0465116 25.9999932,44.0465116 C27.3406172,44.0465116 28.5874913,43.5210732 29.5249906,42.5640247 L45.9687284,25.7969098 C50.9843497,20.6645025 52.0343489,10.9814232 44.9749792,5.0327098 Z" id="Path"></path>
<path d="M46.2718892,3.50241459 C53.7431472,9.79821295 53.7229457,20.7330078 47.4057954,27.197245 L30.9628717,43.9635293 C29.6487574,45.3050462 27.8867501,46.04647 26.009144,46.04647 C24.1408152,46.04647 22.3695113,45.2956437 21.0562304,43.9549771 L4.61132027,27.1866659 C-1.70790147,20.7323686 -1.72137807,9.79370898 5.74484956,3.50371964 C11.4787764,-1.33149882 20.0613911,-1.07727526 26.0092656,3.74139162 C31.9542156,-1.07326776 40.5355381,-1.32491546 46.2718892,3.50241459 Z M27.4365577,7.8409816 L26.009144,9.29542206 L24.5817303,7.8409816 C20.0856339,3.25975559 12.8944426,2.70707366 8.32274593,6.56222409 C2.70796719,11.2924442 2.7181226,19.5353679 7.46832495,24.3871085 L23.9128769,41.155054 C24.4776636,41.731618 25.2204435,42.04647 26.009144,42.04647 C26.8109459,42.04647 27.5432365,41.7383341 28.1062252,41.1636062 L44.5474853,24.3990223 C49.2979771,19.5379178 49.3133455,11.2966718 43.6958622,6.56249388 C39.1208731,2.7129779 31.9290821,3.26339526 27.4365577,7.8409816 Z" fill="#FFFFFF" id="Path"></path>
</g>
</g>
</g>
</g>
</g>
</svg>
</a>
</div>
</article>"""

